package nl.amis;

import ch.qos.logback.classic.Logger;
import javassist.*;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.security.ProtectionDomain;

public class AppTransformer implements ClassFileTransformer {

    private static Logger log = (Logger) LoggerFactory.getLogger(AppTransformer.class);

    private String targetClassName;
    private ClassLoader targetClassLoader;


    public AppTransformer(String targetClassName, ClassLoader targetClassLoader) {
        this.targetClassName = targetClassName;
        this.targetClassLoader = targetClassLoader;
    }


    @Override
    public byte[] transform(ClassLoader loader, String className, Class<?> classBeingRedefined, ProtectionDomain protectionDomain, byte[] classfileBuffer) throws IllegalClassFormatException {
        byte[] byteCode = classfileBuffer;

        String finalTargetClassName = this.targetClassName.replaceAll("\\.", "/"); //replace . with /

        if (!className.equals(finalTargetClassName)) {
            return byteCode;
        }


        if (loader.equals(targetClassLoader) && !finalTargetClassName.equals("nl/amis/MainApplication")) {
            log.info("[Agent] Transforming class {0}", className);
            try {
                ClassPool cp = ClassPool.getDefault();
                CtClass ctClass = cp.get(targetClassName);
                CtMethod[] declaredMethodsList = ctClass.getDeclaredMethods();
                for (CtMethod method : declaredMethodsList) {
                    method.addLocalVariable("beginTime", CtClass.longType);
                    method.insertBefore("beginTime = System.currentTimeMillis();");
                    method.insertBefore(
                            "try {java.nio.file.Path path = java.nio.file.Paths.get(\"log.txt\", new String[0]);" +
                                    "java.nio.file.StandardOpenOption[] optionList = new java.nio.file.StandardOpenOption[1];" +
                                    "optionList[0] = java.nio.file.StandardOpenOption.APPEND;" +
                                    "java.nio.file.Files.write(path, (\"[\" " +
                                    "+ (new java.text.SimpleDateFormat(\"yyyy-MM-dd'T'HH:mm:ss-z\")).format(new java.util.Date()) " +
                                    "+ \"][Thread \" " +
                                    "+ Thread.currentThread().getId() " +
                                    "+ \"] [AGENT] \" " +
                                    "+ this.getClass().getName() " +
                                    "+ \".\" " +
                                    "+ Thread.currentThread().getStackTrace()[1].getMethodName() " +
                                    "+ \" was called.\\n\")" +
                                    ".getBytes()" +
                                    ", optionList);" +
                                    "} " +
                                    "catch (java.io.IOException e) " +
                                    "{e.printStackTrace();}");
                    method.insertAfter(
                            "try {java.nio.file.Path path = java.nio.file.Paths.get(\"log.txt\", new String[0]);" +
                                    "java.nio.file.StandardOpenOption[] optionList = new java.nio.file.StandardOpenOption[1];" +
                                    "optionList[0] = java.nio.file.StandardOpenOption.APPEND;" +
                                    "java.nio.file.Files.write(path, (\"[\" " +
                                    "+ (new java.text.SimpleDateFormat(\"yyyy-MM-dd'T'HH:mm:ss-z\")).format(new java.util.Date()) " +
                                    "+ \"]\" " +
                                    "+ \"[Thread \" " +
                                    "+ Thread.currentThread().getId() " +
                                    "+ \"]\" + \" [AGENT] \" " +
                                    "+ this.getClass().getName() " +
                                    "+ \".\" + Thread.currentThread().getStackTrace()[1].getMethodName() " +
                                    "+ \" took \" " +
                                    "+ (System.currentTimeMillis() - beginTime) " +
                                    "+ \" ms to complete\\n\")" +
                                    ".getBytes()" +
                                    ", optionList);" +
                                    "} " +
                                    "catch (java.io.IOException e) " +
                                    "{e.printStackTrace();}");
                }

                byteCode = ctClass.toBytecode();
                ctClass.detach();
            } catch (NotFoundException e) {
                log.error("NotFoundException:", e);
            } catch (CannotCompileException e) {
                log.error("CannotCompileException:", e);
            } catch (IOException e) {
                log.error("IOException:", e);
            }
        }
        return byteCode;
    }

}

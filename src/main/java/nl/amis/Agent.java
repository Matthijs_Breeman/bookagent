package nl.amis;

import ch.qos.logback.classic.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.lang.instrument.Instrumentation;
import java.nio.file.Files;

public class Agent {

    private Agent() {
        throw new IllegalStateException("Utility class");
    }

    private static Logger log = (Logger) LoggerFactory.getLogger(Agent.class);

    public static void premain(String agentArgs, Instrumentation instrumentation) {
        checkIfFileExists();
        log.info("[Agent] started up in premain.");

        instrumentation.getAllLoadedClasses();

        String[] classList = {
                "nl.amis.Book"
                , "nl.amis.BookFactory"
                , "nl.amis.ChapterTitle"
                , "nl.amis.ChapterTitleFactory"
                , "nl.amis.Title"
                , "nl.amis.MainApplication"
        };

        for (String classname : classList) {
            try {
                transformClass(classname, instrumentation);
            } catch (Exception e) {
                log.error("This class cannot be modified!", e);
            }
        }


        Class[] allLoadedClasses = instrumentation.getAllLoadedClasses();
        long classIncludedCounter = 0;
        long classNotIncludedCounter = 0;
        for (Class clazz: allLoadedClasses) {
            if (clazz.getName().startsWith("nl.amis")) {
                classIncludedCounter++;
                System.out.println("Found loaded class: " + clazz.getName());
            }
            else {
                classNotIncludedCounter++;
            }
        }

        System.out.println("[Agent] found " + classIncludedCounter + " classes startin with nl.amis, and " + classNotIncludedCounter + " classes not starting with nl.amis.");
    }

    private static void checkIfFileExists() {
        String filePath = "log.txt";
        java.io.File file = new java.io.File(filePath);
        java.nio.file.Path path = java.nio.file.Paths.get(filePath);
        try {
            if (!file.exists()) {
                Files.write(path, "".getBytes());
            }
        } catch (IOException e) {
            log.error("File error", e);
        }
    }

    private static void transformClass(String className, Instrumentation instrumentation) throws ClassNotFoundException {
        Class<?> targetClass = null;
        ClassLoader targetClassLoader = null;

        try {
            targetClass = Class.forName(className);
            targetClassLoader = targetClass.getClassLoader();
            transform(targetClass, targetClassLoader, instrumentation);
            return;
        } catch (ClassNotFoundException e) {
            log.error("No class found called with classForName: " + className);
        }

        for (Class<?> clazz : instrumentation.getAllLoadedClasses()) {
            if (clazz.getName().equals(className)) {
                targetClass = clazz;
                targetClassLoader = targetClass.getClassLoader();
                transform(targetClass, targetClassLoader, instrumentation);
                return;
            }
        }
        throw new ClassNotFoundException("[AGENT] Failed to find class [" + className + "]");
    }

    private static void transform(Class<?> targetClass, ClassLoader targetClassLoader, Instrumentation instrumentation) throws ClassNotFoundException {
        AppTransformer appTransformer = new AppTransformer(targetClass.getName(), targetClassLoader);
        instrumentation.addTransformer(appTransformer, true);
        try {
            instrumentation.retransformClasses(targetClass);
        } catch (Exception e) {
            throw new ClassNotFoundException("[AGENT] Transform failed for class: [" + targetClass.getName() + "]", e);
        }
    }
}
